#!/usr/bin/env python
#
#
import os
import click
import numpy as np
from glob import glob
from tqdm import tqdm
from pathlib import Path
from moviepy.editor import (VideoFileClip,
                            concatenate,
                            concatenate_videoclips,
                            TextClip,
                            CompositeVideoClip,
                            ImageClip
                           )
from moviepy.video.tools.segmenting import findObjects

base_size = (1280, 720)
base_fps = 24


def zoomOut(t):
    return 1 - 0.03 * t  # Zoom-in.

def zoomIn(t):
    return 1 + 0.03 * t  # Zoom-in.

def arrive(screenpos, i, nletters):
    """
    Letter animation function
    """
    v = np.array([-1,0])
    d = lambda t : max(0, 3-3*t)
    return lambda t: screenpos-400*v*d(t-0.2*i)

def add_text(clip, text, screensize):
    """
    Function to add moving text to a clip. Pulled from docs:
        https://zulko.github.io/moviepy/examples/moving_letters.html
    """
    txtClip = TextClip(
        text,
        color='white',
        font="Ubuntu-Bold",
        kerning = 5,
        fontsize=100
    )
    cvc = CompositeVideoClip(
        [txtClip.set_position(('left','bottom'))],
        size=screensize
    )
    letters = findObjects(cvc) # a list of ImageClips

    # WE ANIMATE THE LETTERS
    def moveLetters(letters, funcpos):
        return [ letter.set_pos(funcpos(letter.screenpos,i,len(letters)))
                  for i,letter in enumerate(letters)]
    
    ret_clip =  CompositeVideoClip(
        moveLetters(letters, arrive),
        size=screensize
    ).subclip(0,5)
    
    return CompositeVideoClip([clip, ret_clip])

base_dir = '/home/ryan/Videos/canoe-building'
step_dirs = [
    '01-strongback',
    '02-forms',
    '03-inner-stems',
    '04-scarf-joints',
    '05-strips',
    '06-outer-stems',
    '07-sanding',
    '08-outer-epoxy',
    '09-inner-sanding',
    '10-inner-epoxy',
    '11-decking',
    '12-gunwales',
    '13-thwart',
    '14-seats',
    '15-sanding',
    '16-varnishing',
    '17-final',
]

def add_title(screensize):
    """
    Function to add moving text to a clip. Pulled from docs:
        https://zulko.github.io/moviepy/examples/moving_letters.html
    """
    txtClip = TextClip(
        'CanOeVID 2020',
        color='white',
        font="Ubuntu-Bold",
        kerning = 5,
        fontsize=100
    )
    cvc = CompositeVideoClip(
        [txtClip.set_position('center')],
        size=screensize
    )
    letters = findObjects(cvc)

    def moveLetters(letters, funcpos):
        return [ letter.set_pos(funcpos(letter.screenpos,i,len(letters)))
                  for i,letter in enumerate(letters)]
    
    clip =  CompositeVideoClip(
        moveLetters(letters, arrive),
        size=screensize
    ).subclip(0,5)
    
    return clip

def strongback_clip(main_dir, speed=3.0):
    """
    """
    step = '01-strongback'
    text = 'Strongback'
    curr_dir = main_dir / step / '2019_0216_134638_all.mp4'
    clip = VideoFileClip(str(curr_dir))
    clip = add_text(
        clip,
        text,
        clip.size
    ).set_fps(base_fps).speedx(factor=speed)
    return clip

def forms_clip(main_dir, speed=2.0):
    """
    """
    step = '02-forms'
    text = 'Stem Forms'
    curr_dir = main_dir / step / '2019_0217_112917_all.mp4'
    clip = VideoFileClip(str(curr_dir))
    return add_text(clip, text, clip.size).speedx(factor=speed)

def innerstems_clip(main_dir, speed=2.0):
    """
    """
    step = '03-inner-stems'
    text = 'Inner Stems'
    curr_dir = main_dir / step / '2019_0317_102439_001.MP4'
    clip = VideoFileClip(str(curr_dir))
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).resize(base_size).set_fps(base_fps)
    return clip

def scarfjoints_clip(main_dir, speed=2.0):
    """
    """
    step = '04-scarf-joints'
    text = 'Base Strips'
    curr_dir = main_dir / step / '2000_0109_051057_001.MP4'
    clip = VideoFileClip(str(curr_dir))
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).resize(base_size).set_fps(base_fps)
    return clip

def strips_clip(main_dir, speed=20.0):
    """
    """
    step = '05-strips'
    text = 'Strips'
    curr_dir = main_dir / step / '00-basestrips.mp4'
    clip = VideoFileClip(str(curr_dir))
    for r in range(1,11):
        curr_dir = main_dir / step / f"round{r:02}"
        fns = list(curr_dir.glob('*.MP4'))
        fns.sort()
        clip = concatenate_videoclips(
            [clip] + [VideoFileClip(str(f)) for f in fns]
        )
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def outerstems_clip(main_dir, speed=2.0):
    """
    """
    step = '06-outer-stems'
    text = 'Outer Stems'
    curr_dir = main_dir / step / '2000_0106_214227_003.MP4'
    clip = VideoFileClip(str(curr_dir))
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def sanding_clip(main_dir, speed=5.0):
    """
    """
    step = '07-sanding'
    text = 'Outer Hull Sanding'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def outerepoxy_clip(main_dir, speed=3.0):
    """
    """
    step = '08-outer-epoxy'
    text = 'Outer Epoxy'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def innersanding_clip(main_dir, speed=7.0):
    """
    """
    step = '09-inner-sanding'
    text = 'Inner Sanding'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def innerepxoy_clip(main_dir, speed=3.0):
    """
    """
    step = '10-inner-epoxy'
    text = 'Inner Epoxy'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def decking_clip(main_dir, speed=2.0):
    """
    """
    step = '11-decking'
    text = 'Deck Forms & Strips'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def gunwales_clip(main_dir, speed=2.0):
    """
    """
    step = '12-gunwales'
    text = 'Gunwales'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def thwart_clip(main_dir, speed=2.0):
    """
    """
    step = '13-thwart'
    text = 'Thwart'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def seats_clip(main_dir, speed=2.0):
    """
    """
    step = '14-seats'
    text = 'Seats'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def final_sanding_clip(main_dir, speed=2.0):
    """
    """
    step = '15-sanding'
    text = 'Final Sanding'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def varnishing_clip(main_dir, speed=2.0):
    """
    """
    step = '16-varnishing'
    text = 'Varnishing'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip

def final_images(main_dir, speed=1.0):
    """
    """

    # step = '17-final'
    # curr_dir = main_dir / step
    # fns = list(curr_dir.glob('*.jpg'))
    # fns.sort()

    # fns = [fns[0]]

    # slides =[
    #     ImageClip(i.as_posix()).resize(zoomIn).set_position(
    #         ('center', 'center')
    #     ).set_duration(2) for i in fns
    # ]
    # return concatenate(slides, method="compose")
    step = '17-final'
    text = 'Complete!'
    curr_dir = main_dir / step
    fns = list(curr_dir.glob('*.MP4'))
    fns.sort()
    clip = concatenate_videoclips([VideoFileClip(str(f)) for f in fns])
    clip = add_text(
        clip,
        text,
        clip.size
    ).speedx(factor=speed).set_fps(base_fps)
    return clip


@click.command()
@click.option('-f', '--files', help='Base directory for videos',
              default=base_dir, show_default=True)
@click.option('--test', help='Build video, but do not write.', is_flag=True)
def main(files, test):
    """
    Script to concatenate canoe videos and add text.
    """

    p = Path(files)

    clips = [add_title(base_size)]
    with tqdm(total=len(step_dirs), desc='Concatenate Clips') as pbar:
        clips.append(strongback_clip(p))
        pbar.update(1)
        clips.append(forms_clip(p))
        pbar.update(1)
        clips.append(innerstems_clip(p))
        pbar.update(1)
        clips.append(scarfjoints_clip(p))
        pbar.update(1)
        clips.append(strips_clip(p))
        pbar.update(1)
        clips.append(outerstems_clip(p))
        pbar.update(1)
        clips.append(sanding_clip(p))
        pbar.update(1)
        clips.append(outerepoxy_clip(p))
        pbar.update(1)
        clips.append(innersanding_clip(p))
        pbar.update(1)
        clips.append(innerepxoy_clip(p))
        pbar.update(1)
        clips.append(gunwales_clip(p))
        pbar.update(1)
        clips.append(thwart_clip(p))
        pbar.update(1)
        clips.append(decking_clip(p))
        pbar.update(1)
        clips.append(seats_clip(p))
        pbar.update(1)
        clips.append(final_sanding_clip(p))
        pbar.update(1)
        clips.append(varnishing_clip(p))
        pbar.update(1)
        clips.append(final_images(p))
        pbar.update(1)

    final_clip = concatenate_videoclips(clips)

    if not test:
        final_clip.write_videofile(
            "final_clip.mp4",
            fps=25,
            codec='libx264',
            audio=False
        )

if __name__ == '__main__':
    main()
