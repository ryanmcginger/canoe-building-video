#!/bin/bash
#
#
#

export src=/home/ryan/Videos/canoe-building/
export dst=/mnt/pictures/canoe-building/

export opts='-ar --progress --ignore-existing --include=*.MP4 --include=*.jpg'

/usr/bin/rsync ${opts} ${src} ${dst}