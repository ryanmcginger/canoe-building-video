# canoe-building-video

Project for code used to edit my canoe building progress into a single video.

## Dependencies

```bash
# needed for pygame
sudo apt install libportmidi-dev libportmidi-dev libsdl1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev
conda create -f environment.yml
```